# -*- coding: utf-8 -*-
# @Author: Your name
# @Date:   2020-07-09 17:51:03
# @Last Modified by:   Israel Leon @leon12699
# @Last Modified time: 2020-07-09 18:28:31
from .GaussDistribution import Gaussian
from .BinomialDistribution import Binomial